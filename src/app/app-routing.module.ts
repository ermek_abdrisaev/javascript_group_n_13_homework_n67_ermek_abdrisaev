import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MealsComponent } from './meals/meals.component';
import { NewMealComponent } from './new-meal/new-meal.component';
import { NotFoundComponent } from './not-found.component';
import { MealResolverService } from './meals/meal-resolver.service';
import { MealItemComponent } from './meals/meal-item/meal-item.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'meals', component: MealsComponent},
  {path: 'new-meal', component: NewMealComponent},
  {path: ':id/edit', component: NewMealComponent},
  {path: ':id', component: MealItemComponent, resolve: {meal: MealResolverService}},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

