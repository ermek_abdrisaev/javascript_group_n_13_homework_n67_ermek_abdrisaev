import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MealService } from '../shared/meal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-meal',
  templateUrl: './new-meal.component.html',
  styleUrls: ['./new-meal.component.css']
})
export class NewMealComponent implements OnInit, OnDestroy {
  @ViewChild('f') caloriesForm!: NgForm;

  isEdit = false;
  editedId = '';

  isUploading = false;
  mealUploadingSubscription!: Subscription;

  constructor(
    private mealService: MealService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.mealUploadingSubscription = this.mealService.mealUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    });
    this.route.data.subscribe(data =>{
      const meal = <Meal | null>data.meal;

      if(meal){
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          description: meal.description,
          category: meal.category,
          calories: meal.calories,
        })
      }else{
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          description: '',
          category: '',
          calories: '',
        })
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() =>{
      this.caloriesForm.form.setValue(value);
    });
  }

  onSubmit(){
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.caloriesForm.value.description,
      this.caloriesForm.value.category,
      this.caloriesForm.value.calories,
    );

    const mealNext = () => {
      this.mealService.fetchMeals();
      void this.router.navigate(['..', {relativeTo: this.route}]);
    };

    if (this.isEdit){
      this.mealService.editMeal(meal).subscribe(mealNext);
    } else {
      this.mealService.addMeal(meal).subscribe(mealNext)
    }
  }

  ngOnDestroy(): void{
    this.mealUploadingSubscription.unsubscribe();
  }

}
