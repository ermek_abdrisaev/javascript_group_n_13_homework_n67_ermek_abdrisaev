import { Component, Input, OnInit } from '@angular/core';
import { Meal } from '../../shared/meal.model';
import { MealService } from '../../shared/meal.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.css']
})
export class MealItemComponent implements OnInit {
  @Input() meal!: Meal;
  mealId = '';

  constructor(
    private mealService: MealService,
    private http: HttpClient,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.mealId = this.meal.id;

  }


  deleteMeal(id: string){
    this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/meals/${id}.json`)
      .subscribe(() =>{
        this.mealService.fetchMeals();
      });
  }

}
