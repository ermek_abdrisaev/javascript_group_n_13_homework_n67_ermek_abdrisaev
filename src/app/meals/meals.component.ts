import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit, OnDestroy {
  meals!: Meal[];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  loading: boolean = false;

  constructor(private mealService: MealService, private http: HttpClient) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealsChange.subscribe((meals: Meal[]) =>{
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.mealService.fetchMeals();
  }

    getTotalCalories(){
    return this.meals.reduce((acc, c) => {
      return +acc + +c.calories;
    }, 0)
  }

  ngOnDestroy(){
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
  }

}
