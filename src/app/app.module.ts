import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewMealComponent } from './new-meal/new-meal.component';
import { MealsComponent } from './meals/meals.component';
import { MealItemComponent } from './meals/meal-item/meal-item.component';
import { MealService } from './shared/meal.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NewMealComponent,
    MealsComponent,
    MealItemComponent,
    NotFoundComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
