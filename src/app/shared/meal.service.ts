import { HttpClient } from '@angular/common/http';
import { Meal } from './meal.model';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class MealService {
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  private meals: Meal[] = [];

  fetchMeals() {
    this.mealsFetching.next(true);

    this.http.get<{[id: string]: Meal}>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const mealData = result[id];
          return new Meal(id, mealData.description, mealData.category, mealData.calories);
        });

      }))
      .subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice());
        this.mealsFetching.next(false);
      }, () => {
        this.mealsFetching.next(false);
      });
  }

  getMeals() {
    return this.meals.slice();
  }

  fetchMeal(id: string){
    return this.http.get<Meal | null>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/dishes/${id}.json`).pipe(
      map(result =>{
        if(!result){
          return null;
        }
        return new Meal(id, result.description, result.category, result.calories)
      })
    )
  }

  editMeal(meal: Meal){
    this.mealUploading.next(true);
      const body ={
        calories: meal.calories,
        category: meal.category,
        description: meal.description
      };
    return this.http.put(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/dishes/${meal.id}.json`, body).pipe(
      tap( () =>{
        this.mealUploading.next(false);
      }, () =>{
        this.mealUploading.next(false);
      }));
  }

  addMeal(meal: Meal) {
    const body = {
      calories: meal.calories,
      category: meal.category,
      description: meal.description
    };

    this.mealUploading.next(true);

    return this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/meals.json', body)
      .pipe(tap(() => {
          this.mealUploading.next(false);
        }, () => {
          this.mealUploading.next(false);
        })
      );
  }




}
