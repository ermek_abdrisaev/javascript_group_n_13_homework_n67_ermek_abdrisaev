export class Meal {
    constructor(
      public id: string,
      public description: string,
      public category: string,
      public calories: number,
    ){}


}
